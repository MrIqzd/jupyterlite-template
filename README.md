# JupyterLite Demo

JupyterLite deployed as a static site to GitLab Pages, for demo purposes.

## ✨ Try it in your browser ✨

➡️ **https://benabel.gitlab.io/jupyterlite-template**

<!-- ![GitLab-pages](https://user-images.GitLabusercontent.com/591645/120649478-18258400-c47d-11eb-80e5-185e52ff2702.gif) -->

## Upstream

This site is a clone of jupyterlite github pages demo <https://github.com/jupyterlite/demo>.